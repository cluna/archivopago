        static public void UploadMultipleCobranzaSplit(E3.Sale order, string cli, decimal service_charge, List<Sale> listSplit)
        {
            Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "INICIO UploadMultipleCobranzaSplit");
            //RMA
            bool alt = false;
            bool isOmni = false;
            bool isPasilloInfinito = false;
            bool isSFS = false;
            bool isWibond = false;
            string medio = string.Empty;
            string tarjeta = string.Empty;
            bool isAccount_money = false;
            string todasLineas = string.Empty;
            string marketOrigin = string.Empty;
            string medioPago = string.Empty;

            foreach (var o in order.Preferences)
            {
                if (o.Key == "SellerId")
                {
                    if (o.Value == "452702771")
                        alt = true;
                }

                if (o.Key == "CourierId")
                {
                    if (o.Value.ToUpper().Contains("CAC-"))
                        isOmni = true;
                    if (o.Value.ToUpper().Contains("SFS"))
                    {
                        isSFS = true;
                        isOmni = true;
                    }
                }
                if (o.Key == "PasilloInfinito")
                {
                    isPasilloInfinito = true;
                }
            }
            if (order.UserData.ProfileId == 1)
                return;
            bool isCuotaACuota = false;
            bool isBapro = false;
            bool isGateway = false;
            string lote = "";
            string numAuto = "";
            string platico = "";
            string cardName = "";
            decimal ammountTot = 0;
            bool flagArrayPaymentsData = false;
            string lastCode = "";
            bool isTransferencia = false;
            bool isEcheq = false;
            bool isAhora = false;

            Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "BUSCA DATOS PAGO");

            //string mpJson = string.Empty;
            List<string> ArrayPayment = new List<string>();
            foreach (var o in order.Preferences)
            {
                if (o.Key == "PaymentsData2" || o.Key == "PaymentsData")
                    ArrayPayment.Add(o.Value);
            }

            Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN BUSCAR DATOS PAGO");

            if (ArrayPayment.Count > 0)
            {
                flagArrayPaymentsData = true;
                var ListGatewayOrderNumber = order.GatewayOrderNumber.Trim().Split(' ');
                int ContResp = 0;
                foreach (var mpJson in ArrayPayment)
                {
                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "INICIO NUMCOMPRO Y NRO DE PAGO");
                    string nropago = string.Empty;
                    string numCompro = string.Empty;
                    if (order.InternalCode.Contains("-"))
                        numCompro = order.InternalCode.Substring(4, 13);
                    else if (!string.IsNullOrEmpty(order.InternalCode))
                        numCompro = order.InternalCode;
                    else
                        numCompro = order.Id.ToString();

                    string cuotas = order.Payments.ToString();

                    try
                    {
                        if (ContResp > ListGatewayOrderNumber.Length)
                            continue;
                        //ContResp=0
                        string sGatewayOrderNumber = ListGatewayOrderNumber[ContResp];

                        if (!string.IsNullOrEmpty(sGatewayOrderNumber))
                            nropago = sGatewayOrderNumber.Replace("\r", string.Empty).Replace("\n", string.Empty);

                        Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN NUMCOMPRO: " + numCompro + " NROPAGO: " + nropago);

                        if (mpJson.ToString().Contains(_spsSearchKey))
                        {
                            isGateway = true;
                            /*decidir*/
                            isAhora = mpJson.Contains("Flag Ahora12 True");
                            string dat1 = mpJson.ToString().Replace("{\"response\":", string.Empty);
                            dat1 = dat1.Remove(dat1.Length - 1);
                            string[] parts = dat1.Replace("]??[", "]¨[").Split('¨');
                            //TODO: Parametrizar y/o mejorar esta condición
                            isCuotaACuota = parts[0].Contains("\\\"IDSITE\\\":\\\"00140136\\\"");
                            isBapro = parts[0].Contains("\\\"IDSITE\\\":\\\"00030843\\\"") || parts[0].Contains("\\\"IDSITE\\\":\\\"00040843\\\"");
                            var pmn_listt = Newtonsoft.Json.JsonConvert.DeserializeObject<List<E3.VtexPaymentsChot>>(parts[1]);

                            if (order.InternalCode.Contains("MKB"))
                            {
                                marketOrigin = order.Preferences.Where(x => x.Key == "marketOrigin").FirstOrDefault().Value;
                            }

                            foreach (var pmnt in pmn_listt)
                            {
                                string connector = string.Empty;
                                string authenticationUrl = string.Empty;
                                foreach (var k in pmnt.fields)
                                {
                                    switch (k.name)
                                    {
                                        case "connector": connector = k.value; break;
                                        case "authenticationUrl": authenticationUrl = k.value; break;

                                    }
                                }
                                if (!connector.Contains("DecidirV1") && !authenticationUrl.ToLower().Contains("wibond") && !mpJson.Contains("promissory"))
                                    continue;

                                string sequence = string.Empty;
                                string firstdigits = string.Empty;
                                string lastdigits = string.Empty;
                                int paymentSystem = pmnt.paymentSystem;
                                string paymentSystemName = pmnt.paymentSystemName;
                                string group = pmnt.group;

                                if (paymentSystemName.ToUpper().Contains("TRANSFERENCIA"))
                                {
                                    isTransferencia = true;
                                }
                                else if (paymentSystemName.ToUpper().Contains("ECHEQ"))
                                {
                                    isEcheq = true;
                                }
                                foreach (var k in pmnt.fields)
                                {
                                    switch (k.name)
                                    {
                                        case "firstDigits": firstdigits = k.value; break;
                                        case "lastDigits": lastdigits = k.value; break;
                                        //  case "paymentSystemName": paymentSystemName = k.value; break;
                                        //    case "group": group = k.value; break;
                                        case "sequence": sequence = k.value; break;
                                    }
                                }
                                if (sequence == lastCode)
                                    continue;


                                lastCode = sequence;
                                if ((authenticationUrl.ToLower().Contains("wibond") || paymentSystem == 203 || paymentSystem == 202) && order.SiteId != 3)
                                {
                                    isWibond = true;
                                    cardName = "wibond";
                                }
                                else if (group.Contains("debit"))
                                {
                                    cardName = "debvisa";
                                    if (paymentSystemName.ToUpper().Contains("MAESTRO"))
                                        cardName = "maestro";
                                }
                                else
                                {
                                    cardName = "XXXX";
                                    if (paymentSystemName.ToUpper().Contains("MASTER"))
                                        cardName = "master";
                                    if (paymentSystemName.ToUpper().Contains("VISA"))
                                        cardName = "visa";
                                    if (paymentSystemName.ToUpper().Contains("AME"))
                                        cardName = "amex";
                                    if (paymentSystemName.ToUpper().Contains("NARANJA"))
                                        cardName = "naranja";
                                    if (paymentSystemName.ToUpper().Contains("DINERS"))
                                        cardName = "diners";
                                    if (paymentSystemName.ToUpper().Contains("CABAL"))
                                        cardName = "cabal";

                                }
                                if (!isTransferencia && !isEcheq)
                                {
                                    platico = firstdigits + "####" + lastdigits;
                                    lote = pmnt.connectorResponse.nsu;
                                    numAuto = pmnt.connectorResponse.authId;
                                    DateTime fechaDesde = new DateTime(2022, 12, 31);
                                    if (isAhora && order.CreationDate > fechaDesde)
                                    {
                                        switch (pmnt.installments.ToString())
                                        {
                                            case "3":
                                                cuotas = "13";
                                                break;
                                            case "6":
                                                cuotas = "16";
                                                break;
                                            case "10":
                                                cuotas = "40";
                                                break;
                                            case "12":
                                                cuotas = "7";
                                                break;
                                            case "18":
                                                cuotas = "8";
                                                break;
                                            case "24":
                                                cuotas = "25";
                                                break;
                                            case "30":
                                                cuotas = "31";
                                                break;
                                            default:
                                                cuotas = pmnt.installments.ToString();
                                                break;
                                        }
                                    }
                                    else
                                    {
                                        cuotas = pmnt.installments.ToString();
                                    }
                                    ammountTot = (decimal)pmnt.value / (decimal)100;
                                }
                                break;
                            }

                        }
                        else
                        {

                            try
                            {
                                if (!string.IsNullOrEmpty(mpJson.ToString()))
                                {
                                    var paydesa = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, object>>(mpJson.ToString());
                                    var paydes = (Newtonsoft.Json.Linq.JObject)paydesa["response"];
                                    var trasnsacion_details = (Newtonsoft.Json.Linq.JObject)paydes["transaction_details"];
                                    medio = "2001";
                                    tarjeta = "MERCADO PAGO";
                                    string mpData = string.Empty;
                                    if (paydes["processing_mode"] != null && paydes["processing_mode"].ToString() == "gateway")
                                    {
                                        var card = (Newtonsoft.Json.Linq.JObject)paydes["card"];
                                        platico = card["first_six_digits"].ToString() + "####" + card["last_four_digits"].ToString();
                                        isGateway = true;
                                        cardName = paydes["payment_method_id"].ToString();


                                        mpData = Helper.Get("https://api.mercadopago.com/v1/payments/" + sGatewayOrderNumber + "?access_token=" + _mpAccessToken, "GET", string.Empty, _logfilename);


                                        var paydesLive = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<object, object>>(mpData);
                                        var pAccReq = (Newtonsoft.Json.Linq.JArray)paydesLive["acquirer_reconciliation"];
                                        string acq = paydesLive["acquirer"] != null ? paydesLive["acquirer"].ToString() : "error";
                                        foreach (var reqData in pAccReq)
                                        {
                                            var objDes = (Newtonsoft.Json.Linq.JObject)reqData;
                                            if (objDes["batch_number"] != null && !string.IsNullOrEmpty(objDes["batch_number"].ToString()))
                                                lote = objDes["batch_number"].ToString();
                                            if (objDes["authorization_code"] != null && !string.IsNullOrEmpty(objDes["authorization_code"].ToString()))
                                                numAuto = objDes["authorization_code"].ToString();
                                        }
                                        if (acq == "firstdata" && string.IsNullOrEmpty(lote))
                                        {
                                            lote = "";
                                        }

                                    }
                                    else if (paydes["processing_mode"] != null && paydes["processing_mode"].ToString() == "aggregator" && paydes["payment_type_id"].ToString() != "account_money")
                                    {

                                        isGateway = false;

                                        cuotas = paydes["installments"].ToString();
                                    }
                                    else if (paydes["payment_type_id"] != null && paydes["payment_type_id"].ToString() == "account_money")
                                    {

                                        string mpAccessToken = System.Configuration.ConfigurationManager.AppSettings["MP:AccessTokenMIRGORPRO"];
                                        mpData = Helper.Get("https://api.mercadopago.com/v1/payments/" + paydes["id"].ToString() + "?access_token=" + mpAccessToken, "GET", string.Empty, _logfilename);


                                        cardName = paydes["payment_method_id"].ToString();
                                        isGateway = false;
                                        cuotas = paydes["installments"].ToString();


                                    }

                                    try
                                    {
                                        if (trasnsacion_details["total_paid_amount"] != null)
                                        {
                                            ammountTot = decimal.Parse(trasnsacion_details["total_paid_amount"].ToString().Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        Console.WriteLine("No pudo parsear trasnsacion_details[\"total_paid_amount\"]" + trasnsacion_details["total_paid_amount"] + ex.Message);
                                        Helper.LogLine(_logfilename, "ERROR OP " + order.Id.ToString(), "No pudo parsear trasnsacion_details[\"total_paid_amount\"]" + trasnsacion_details["total_paid_amount"] + ex.Message);
                                    }

                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("No pudo extrar los datos correctamente de mercadopago" + ex.Message);
                                Helper.LogLine(_logfilename, "ERROR OP " + order.Id.ToString(), "No pudo extrar los datos correctamente de mercadopago" + ex.Message);
                            }



                        }

                    }
                    catch (Exception exxx)
                    {
                        Console.WriteLine("Imposible subir constancia UploadMultipleCobranza" + exxx.Message);
                        Helper.LogLine(_logfilename, "ERROR OP " + order.Id.ToString(), "No se pudo cargar constancia de pago en presea: " + exxx.Message);
                        return;
                    }

                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "INICIO CLIENTE Y MEDIOS PAGO");
                    string client = cli;
                    string fecha = (order.CreationDate < DateTime.Now.AddDays(-8) || !order.CreationDate.Month.Equals(DateTime.Now.Month)) ? DateTime.Now.ToString("dd/MM/yyyy") : order.CreationDate.ToString("dd/MM/yyyy");

                    if (string.IsNullOrEmpty(cardName) && tarjeta != "MERCADO PAGO")
                    {
                        medio = "0000";
                        tarjeta = "XXXX";
                    }

                    if (order.PayMethodId == _todoPagoId)
                    {
                        medio = "2002";
                    }
                    if (isGateway && !medio.Contains("2001"))
                    {
                        switch (cardName)
                        {
                            case "visa": { medio = "1101"; tarjeta = "VISA"; } break;
                            case "amex": { medio = "1102"; tarjeta = "AMERICAN EXPRESS"; } break;
                            case "master": { medio = "1103"; tarjeta = " MASTER CARD"; } break;
                            case "naranja": { medio = "1106"; tarjeta = "NARANJA"; } break;
                            case "cabal": { medio = "1108"; tarjeta = "CABAL"; } break;
                            case "nativa": { medio = "1109"; tarjeta = "NATIVA"; } break;
                            case "diners": { medio = "1111"; tarjeta = "DINERS"; } break;
                            case "debvisa": { medio = "1201"; tarjeta = "VISA DEBITO"; } break;
                            case "debmaster": { medio = "1203"; tarjeta = "MASTER CARD DEBITO"; } break;
                            case "maestro": { medio = "1204"; tarjeta = "MAESTRO"; } break;
                            case "debcabal": { medio = "1208"; tarjeta = "CABAL 24"; } break;
                            case "wibond": { medio = "2300"; tarjeta = "WIBOND"; } break;
                            case "account_money": { medio = "2006"; tarjeta = "MERCADO PAGO DINERO EN CUENTA"; } break;
                            default: { medio = "0000"; tarjeta = "XXXX"; } break;
                        }

                        //MODIFICACIÖN SITE CUOTA A CUOTA
                        //VISA: 2700 y MASTER: 2701
                        if (isCuotaACuota)
                        {
                            switch (cardName)
                            {
                                case "visa": { medio = "2700"; tarjeta = "VISA"; } break;
                                case "master": { medio = "2701"; tarjeta = "MASTER CARD"; } break;
                            }
                        }
                        if (isBapro)
                        {
                            switch (cardName)
                            {
                                case "visa": { medio = "1210"; tarjeta = "VISA BAPRO"; } break;
                                case "master": { medio = "1211"; tarjeta = "MASTER BAPRO"; } break;
                            }
                        }

                    }
                    if (order.PayMethodId == 11)
                    {
                        medio = "4001";
                        tarjeta = "PROVENCRED";
                    }

                    if (cardName == "account_money")
                    {
                        medio = "2006";
                        tarjeta = "MERCADO PAGO DINERO EN CUENTA";
                        isAccount_money = true;
                    }
                    if (isTransferencia)
                    {
                        medio = "900";
                    }
                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN CLIENTE: " + client + "Y MEDIOS PAGO: " + medio + " - " + tarjeta);
                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "INICIO SERVICE CHARGE: " + service_charge + " Y AMMOUNT");
                    //decimal ammount = order.Total + service_charge;
                    decimal ammount = ammountTot;

                    if (ammount <= 0)
                    {
                        Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "AMOUNT < 0 --> " + ammount);
                        return;
                    }

                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN SERVICE CHARGE Y AMMOUNT: " + ammount);

                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "INICIO ARMAR LINEA");
                    string line = string.Empty;
                    if (isWibond && !isPasilloInfinito && !isAccount_money)
                    {
                        line = client.Trim() + "," + fecha.Trim() + "," + ammount.ToString(System.Globalization.CultureInfo.InvariantCulture).Replace(',', '.') + "," + medio + "," + numCompro + "," + tarjeta + "," + numCompro + ",,,";
                    }
                    else if (isAccount_money || isPasilloInfinito)
                    {
                        line = client.Trim() + "," + fecha.Trim() + "," + ammount.ToString(System.Globalization.CultureInfo.InvariantCulture).Replace(',', '.') + "," + medio + "," + order.GatewayOrderNumber + "," + tarjeta + "," + numCompro + ",,,";
                    }
                    else if (order.SiteId == 3)
                    {
                        if (isEcheq || isTransferencia)//VALIDAR ISPROMISSORY
                        {
                            var vto = order.Preferences.Where(x => x.Key == "FechaPagoComprobante").FirstOrDefault().Value;
                            var NumeroComprobante = order.Preferences.Where(x => x.Key == "NumeroComprobante").FirstOrDefault().Value;

                            line = client.Trim() + "," + fecha.Trim() + "," + ammount.ToString(System.Globalization.CultureInfo.InvariantCulture).Replace(',', '.') + "," + medio + "," + NumeroComprobante + "," + "TRANSF HSBC" + "," + numCompro
                            + ",,,,"/*nro_lote,numtarj,cod_auto*/ + "1" + "," + order.GatewayOrderNumber + "," + vto + ",7000,1124";
                        }
                        else
                        {
                            line = client.Trim() + "," + fecha.Trim() + "," + ammount.ToString(System.Globalization.CultureInfo.InvariantCulture).Replace(',', '.') + "," + medio + "," + nropago.Replace("-", string.Empty) + "," + tarjeta + "," + numCompro
                            + ",,"/*nro_lote*/ + platico + ",,"/*cod_auto*/ + cuotas + "," + order.GatewayOrderNumber + ",,7000,1124";
                        }
                    }
                    else
                    {
                        if (order.InternalCode.Contains("MKB"))
                        {
                            line = client.Trim() + "," + fecha.Trim() + "," + ammount.ToString(System.Globalization.CultureInfo.InvariantCulture).Replace(',', '.') + "," + medio + "," + lote + "," + tarjeta + "," + numCompro;
                        }
                        else
                        {
                            line = client.Trim() + "," + fecha.Trim() + "," + ammount.ToString(System.Globalization.CultureInfo.InvariantCulture).Replace(',', '.') + "," + medio + "," + nropago.Replace("-", string.Empty) + "," + tarjeta + "," + numCompro;
                        }
                        if (isGateway && !order.InternalCode.Contains("MKB"))
                        {
                            line += "," + lote + "," + numAuto + "," + platico;
                        }
                        else if (order.InternalCode.Contains("MKB"))
                        {
                            line += ",," + numAuto + "," + platico;
                        }
                    }
                    if (order.SiteId != 3)
                    {
                        line += "," + cuotas;
                        line += "," + order.GatewayOrderNumber;
                    }

                    if (order.InternalCode.Contains("MKB"))
                    {
                        line += ",7000,1272";
                    }
                    line = line.Trim() + Environment.NewLine;
                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN ARMAR LINEA: " + line);
                    medioPago += tarjeta;

                    Ftp ftp = new Ftp(_ftpHost, _ftpUser, _ftpPass);
                    ftp.usePasive = true;
                    byte[] byteArray = Encoding.UTF8.GetBytes(line);
                    System.IO.MemoryStream stream = new System.IO.MemoryStream(byteArray);
                    string filenameUpload = "e_cobranzas.txt";
                    if (isGateway)
                    {
                        filenameUpload = "e_cobranzas_gw.txt";

                    }
                    string ftpfolder = "/Mirgor/COBRO_WEB/";
                    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolder:Site" + order.SiteId]))
                    {
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolder:Site" + order.SiteId];
                    }
                    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFile:Site" + order.SiteId]))
                    {
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFile:Site" + order.SiteId];
                    }
                    if (order.SiteId == 0 && alt && !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileAlt"]))
                    {
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileAlt"];
                    }
                    //OMNICHANNEL
                    if (isOmni && !isSFS && !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileOm"]))
                    {
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileOm"];
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderOm"];
                    }
                    //SHIP FROM STORE
                    if (isOmni && isSFS && !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileOmSFS"]))
                    {
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileOmSFS"];
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderOmSFS"];
                    }
                    //PICKUP
                    if (order.ShippingMethod != null && order.ShippingMethod.ShippingTypeEnum == 1 && !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPickUp"]))
                    {
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPickUp"];
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFilePickUp"];
                    }
                    //PASILLO INFINITO
                    if (isPasilloInfinito && !string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPasilloInfinito"]))
                    {
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPasilloInfinito"];
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFilePasilloInfinito"];
                    }
                    //NUBI
                    if (order.PayMethodId == 11)
                    {
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolder"];
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFile"];
                    }
                    //SMB B2B
                    if (order.SiteId == 3)
                    {
                        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPortalB2B"]))
                        {
                            ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPortalB2B"];
                        }
                        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFilePortalB2B"]))
                        {
                            filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFilePortalB2B"];
                        }
                    }
                                        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:Canal:Site0"]) && System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:Canal:Site0"] == "NU")
                    {
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFile"];
                    }


                    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolder:Site" + order.SiteId]))
                    {
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolder:Site" + order.SiteId];
                    }
                    if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFile:Site" + order.SiteId]))
                    {
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFile:Site" + order.SiteId];
                    }

                    if (order.InternalCode == "TODISK" /*|| 1 == 1*/)
                    {

                        System.IO.File.AppendAllText("C:\\testcaca\\" + filenameUpload, line);
                        Console.WriteLine("Se grabo en el archivo de presea2");
                        return;
                    }
                    if (order.InternalCode == "TEST-IGNORE")
                        filenameUpload = "TEST-IGNORE.txt";
                    //SMB B2B
                    if (order.SiteId == 3)
                    {
                        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPortalB2B"]))
                        {
                            ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderPortalB2B"];
                        }
                        if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFilePortalB2B"]))
                        {
                            filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFilePortalB2B"];
                        }
                    }
                    // DIGGIT
                    var isDiggit = IsDiggit(order);

                    if (isDiggit)
                    {
                        filenameUpload = DiggitFtpFile;
                        ftpfolder = DiggitFtpFolder;
                    }


                    if (order.InternalCode.Contains("MKB"))
                    {
                        ftpfolder = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFolderMarketplaces"].Replace("XY", marketOrigin);
                        filenameUpload = System.Configuration.ConfigurationManager.AppSettings["Neuralsoft:FtpFileMarketplaces"].Replace("XY", marketOrigin);
                    }

                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "CARPETA FRP: " + ftpfolder + " - NOMBRE ARCHIVO: " + filenameUpload);
                    try
                    {
                        Helper.LogLine("LogCarpetasFTP.log", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "Id: " + order.Id + " - InternalCode: " + order.InternalCode + " - A subir a ftp: " + ftpfolder + "/" + filenameUpload);
                        Console.WriteLine("Ftp folder: " + ftpfolder + " - filenameUpload: " + filenameUpload);
                        ftp.append(ftpfolder + filenameUpload, stream);
                        Helper.LogLine("LogCarpetasFTP.log", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "Id: " + order.Id + " - InternalCode: " + order.InternalCode + " - Subimos a ftp: " + ftpfolder + "/" + filenameUpload);
                        SaveFileLocalPresea(filenameUpload, line);
                        Console.WriteLine("Se guardó en el archivo try");
                    }
                    catch
                    {
                        Helper.LogLine("LogCarpetasFTP-Catch.log", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "Id: " + order.Id + " - InternalCode: " + order.InternalCode + " - A subir a ftp: " + ftpfolder + "/" + filenameUpload);
                        ftp.upload(ftpfolder + filenameUpload, stream);
                        Helper.LogLine("LogCarpetasFTP-Catch.log", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"), "Id: " + order.Id + " - InternalCode: " + order.InternalCode + " - Subimos a ftp: " + ftpfolder + "/" + filenameUpload);
                        SaveFileLocalPresea(filenameUpload, line);
                        Console.WriteLine("Se guardó en el archivo catch");
                    }

                    Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN iteracion");
                    ContResp += 1;
                    todasLineas += line + "\n";

                }
                Helper.LogLine("ArchivosPago.log", "OP ID: " + order.Id + "INTERNALCODE: " + order.InternalCode, "FIN UploadMultipleCobranzaSplit");
                Console.WriteLine("Se actualiza la operación en bd");

                order.SubStatus = 1;

                if (order.Preferences.Any(x => x.Key == "ArchivoDePago"))
                    order.Preferences.Where(x => x.Key == "ArchivoDePago").FirstOrDefault().Value = "SI";
                else
                    order.Preferences.Add(new E3.Preference() { Key = "ArchivoDePago", Value = "SI" });


                if (todasLineas.EndsWith("\r\n"))
                {
                    todasLineas = todasLineas.Remove(todasLineas.Length - 2);

                }
                else if (todasLineas.EndsWith("\n") || todasLineas.EndsWith("\r"))
                {
                    todasLineas = todasLineas.Remove(todasLineas.Length - 1);
                }


                if (order.Preferences.Any(x => x.Key == "LineaArchivoDePago"))
                    order.Preferences.Where(x => x.Key == "LineaArchivoDePago").FirstOrDefault().Value = todasLineas.TrimEnd(new char[] { '\\', 'n' });
                else
                    order.Preferences.Add(new E3.Preference() { Key = "LineaArchivoDePago", Value = todasLineas.TrimEnd(new char[] { '\\', 'n' }) });



                var saleBody = Helper.Serialize<E3.Sale>(order);
                resAuthStatic = RegenerateToken(resAuthStatic, 2);

                string res_s = Helper.Get(_urlEndpoint + "Sale/?id=" + order.Id + "&hash=" + resAuthStatic.data, "PUT", saleBody, _logfilename);


                while (res_s.Contains("INVALID HASH"))
                {
                    resAuthStatic = RegenerateToken(resAuthStatic, 2);
                    res_s = Helper.Get(_urlEndpoint + "Sale/?id=" + order.Id + "&hash=" + resAuthStatic.data, "PUT", saleBody, _logfilename);
                }



                if (flagArrayPaymentsData == false)
                {
                    Console.WriteLine("No se grabo el archivo de presea");
                }


                return;


            }

        }